package javadoc1;

/**
 * To perform basic string functions
 * @author Niluxy
 * @since 21/05/2020
 * @version 1.0.0v
 */

public class ReplaceWord {
	/**
	* This is the main method of this program
	* @param args for adding arguments
	*/
	
	public static void main(String args[]) {
		/** 
		* String variable text,string of the word
		*/
	      String Str = new String("be Confident");

	    
	      /**
	  	* print the return value of string after replace the word
	  	*/
	     
	      System.out.print("Return Value :" );
	      System.out.println(Str.replace('e', 'P'));
	}
}
/*

/*
output
Return Value :bP ConfidPnt
/*