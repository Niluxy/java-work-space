package prog.CSD.string;

public class PractiseString {
	public static void main(String args[]) {
	String a="become a succefull woman";
	char result=a.charAt(9);
	System.out.println("QUESTION 1:-charAt Method");
	System.out.println(a);
	System.out.println(result);
	System.out.println("----------------------------");
	
	String name1="i want to";
	String name2="become a successfull woman";
	System.out.println("QUESTION 5:-Concat Method");
	System.out.println(name1+" "+name2);
	String b=name1.concat(name2);
	System.out.println(b);
	System.out.println("----------------------------");
	
	String c=new String("become a successfull woman");
	System.out.println("QUESTION 16:-indexOf (int ch )Method");
	System.out.println(c);
	System.out.println(c.indexOf("n"));
	System.out.println("----------------------------");
	
	String d=new String("become a successfull woman");
	System.out.println("QUESTION 17:-indexOf(int ch, int fromIndex) Method");
	System.out.println(d);
	System.out.println(d.indexOf("s",5));
	System.out.println("----------------------------");
	
	String e=new String("become a successfull woman");
	String e1=new String("woman");
	System.out.println("QUESTION 18:-indexOf (String str) Method");
	System.out.println(e);
	System.out.println(e1);
	System.out.println(e.indexOf(e1));
	System.out.println("----------------------------");
	
	String f=new String("become a successfull woman");
	String f1=new String("woman");
	System.out.println("QUESTION 19:-indexOf (String str,int fromindex) Method");
	System.out.println(f);
	System.out.println(f1);
	System.out.println(f.indexOf(f1,6));
	System.out.println("----------------------------");
	
	String g=new String("become a successfull woman");
	System.out.println("QUESTION 21:-last indexOf (int ch) Method");
	System.out.println(g);
	System.out.println(g.indexOf("a"));
	System.out.println("----------------------------");

	String h=new String("become a successfull woman");
	System.out.println("QUESTION 22:-last indexOf (int ch,int fromindex) Method");
	System.out.println(h);
	System.out.println(h.indexOf("m",9));
	System.out.println("----------------------------");

	String i=new String("become a successfull woman");
	String i1=new String("woman");
	System.out.println("QUESTION 23:-last indexOf (String str) Method");
	System.out.println(i);
	System.out.println(i.indexOf(i1));
	System.out.println("----------------------------");

	String j=new String("become a successfull woman");
	String j1=new String("woman");
	System.out.println("QUESTION 24:-last indexOf (String str,int fromindex) Method");
	System.out.println(j);
	System.out.println(j.indexOf(j1,6));
	System.out.println("----------------------------");
	
	String k=new String("become a successfull woman");
	System.out.println("QUESTION 25:-int length Method");
	System.out.println(k);
	System.out.println(k.length());
	System.out.println("----------------------------");

	String l=new String("become a successfull woman");
	System.out.println("QUESTION 29:-string replace Method");
	System.out.println(l);
	System.out.println(l.replace("b","n"));
	System.out.println("----------------------------");


	String m=new String("become a successfull woman");
	System.out.println("QUESTION 34:-string startsWith Method");
	System.out.println(m);
	System.out.println(m.startsWith("become"));
	System.out.println("----------------------------");
	

	String n=new String("become a successfull woman");
	System.out.println("QUESTION 37:-string substring Method");
	System.out.println(n);
	System.out.println(n.substring(9));
	System.out.println("----------------------------");


	String o=new String("become a successfull woman");
	System.out.println("QUESTION 38:-string substring (beginIndex,endIndex) Method");
	System.out.println(o);
	System.out.println(o.substring(2,7));
	System.out.println("----------------------------");

	String p=new String("become successfull woman");
	System.out.println("QUESTION 40:-string toLowerCase Method");
	System.out.println(p);
	System.out.println(p.toLowerCase());
	System.out.println("----------------------------");

	String q=new String("become a successfull woman");
	System.out.println("QUESTION 43:-string toUpperCase Method");
	System.out.println(q);
	System.out.println(q.toUpperCase());
	System.out.println("----------------------------");
	
	String s=new String("become a successfull woman");
	System.out.println("QUESTION 45:-string Trim Method");
	System.out.println(s);
	System.out.println(s.trim());
	System.out.println("----------------------------");
}

}
